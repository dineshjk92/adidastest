package pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

@JsonIgnoreProperties
@Builder
public class ProductPojo {

    private Object id;
    private String name;
    private String description;

    ProductPojo() {}

    public ProductPojo(Object id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public void setId(Object id) {
        this.id = id;
    }

    public void setname(String name) {
        this.name = name;
    }

    public void setdescription(String description) {
        this.description = description;
    }

    public Object getId() {
        return id;
    }

    public String getname() {
        return name;
    }

    public String getdescription() {
        return description;
    }

}
