package pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

@EqualsAndHashCode
@ToString
@JsonIgnoreProperties
public class ProductCompletePojo {

    private Object id;
    private String name;
    private String description;
    private String currency;
    private int price;

    ProductCompletePojo() {}

    public ProductCompletePojo(Object id, String name, String description, String currency, int price ) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.currency = currency;
        this.price = price;
    }

    public void setId(Object id) {
        this.id = id;
    }

    public void setname(String name) {
        this.name = name;
    }

    public void setdescription(String description) {
        this.description = description;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Object getId() {
        return id;
    }

    public String getname() {
        return name;
    }

    public String getdescription() {
        return description;
    }

    public String getCurrency() {
        return currency;
    }

    public int getPrice() {
        return price;
    }

}
