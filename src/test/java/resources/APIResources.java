package resources;

public enum APIResources {
	AddProductAPI("/product"),
	GetProductAPI("/product/{id}"),
	GetAllProductAPI("/product/"),
	UpdateProductAPI("/product/{id}");

	private String resource;
	
	APIResources(String resource) {
		this.resource=resource;
	}
	
	public String getResource() {
		return resource;
	}
}
