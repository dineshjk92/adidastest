@UpdateProduct @All
Feature: Verify updating product details from the product-service


  Scenario Outline: Verify updating a product by its product id
    Given User calls Add Product API with "<id>", "<name>", "<description>" via POST http request
    Then Status code <statusCode>
    And Verify the response contains "<id>", "<name>", "<description>"
    When User calls Get Product API with the same ID via GET http request
    Then Status code <statusCode>
    And Verify the response contains "<id>", "<name>", "<description>"
    When User calls update product API with the same ID via PUT http request by updating name "<update_name>" and description "<update_description>"
    Then Status code <statusCode>
    And The response should contain the name "<update_name>" and description "<update_description>"
    Examples:
      | id        | statusCode | update_name      | update_description |
      | AutoTest_ | 200        | update_auto_test | update_auto_test   |

  Scenario Outline: Verify updating a non-existing product
    When User calls update product API with a non-existing id "<id>"
    Then Status code <statusCode>
    And The response should not contain the id "<id>"
    Examples:
      | id         | statusCode |
      | 1129948400 | 400        |


