@AddProduct @All
Feature: Verify adding a product to the product-service


  Scenario Outline: Verify adding a product
    Given User calls Add Product API with "<id>", "<name>", "<description>" via POST http request
    Then Status code <statusCode>
    And Verify the response contains "<id>", "<name>", "<description>"
    When User calls Get Product API with the same ID via GET http request
    Then Status code <statusCode>
    And Verify the response contains "<id>", "<name>", "<description>"
    Examples:
      | id        | name         | description               | statusCode |
      | AutoProd1 | Auto_Product | product added by autotest | 200        |

  Scenario Outline: Verify adding a product with empty name and description
    Given User calls Add Product API with "<id>" with empty name and description via POST http request
    Then Status code <statusCode>
    And Verify the response contains "<id>"
    And Verify the name and description are empty in the response
    When User calls Get Product API with the same ID via GET http request
    Then Status code <statusCode>
    And Verify the response contains "<id>"
    Examples:
      | id        | statusCode | id     |
      | AutoProd1 | 200        | status |

  Scenario Outline: Verify adding a product with null values for name and description
    Given User calls Add Product API with "<id>" with null name and description via POST http request
    Then Status code <statusCode>
    And Verify the response contains "<id>"
    And Verify the name and description are null in the response
    When User calls Get Product API with the same ID via GET http request
    Then Status code <statusCode>
    And Verify the response contains "<id>"
    Examples:
      | id        | statusCode | id     |
      | AutoProd1 | 200        | status |

  Scenario Outline: Verify adding a product with existing product id
    Given User calls Add Product API with "<id>", "<name>", "<description>" via POST http request
    Then Status code <statusCode>
    And Verify the response contains "<id>", "<name>", "<description>"
    When User calls Get Product API with the same ID via GET http request
    Then Status code <statusCode>
    And Verify the response contains "<id>", "<name>", "<description>"
    When User calls Add Product API with the same details via POST http request
    Then Verify the response does not contain "<id>", "<name>", "<description>"
    Examples:
      | id        | name         | description               | statusCode | id     |
      | AutoProd1 | Auto_Product | product added by autotest | 200        | status |

  Scenario Outline: Verify adding a product with only product id
    Given User calls Add Product API with only ID "<id>"
    Then Status code <statusCode>
    And Verify the response contains "<id>"
    And Verify the response does not contain "name" and "description"
    When User calls Get Product API with the same ID via GET http request
    Then Status code <statusCode>
    And Verify the response contains "<id>"
    Examples:
      | id        | statusCode |
      | AutoTest_ | 200        |

  Scenario Outline: Verify adding a product without product id
    Given User calls Add Product API without product id with "<name>" and "<description>"
    Then Status code <statusCode>
    And Verify the response does not contain "<name>" and "<description>"
    And Verify the response does not contain "id"
    Examples:
      | name     | description | statusCode |
      | AutoTest | AutoTest    | 200        |

  Scenario Outline: Verify adding a product with product id having 1000 chars
    Given User calls Add Product API with product id having 1000 chars
    Then Status code <statusCode>
    And Verify the response does not contain "<name>" and "<description>"
    And Verify the response does not contain "id"
    Examples:
      | name     | description | statusCode |
      | AutoTest | AutoTest    | 200        |
