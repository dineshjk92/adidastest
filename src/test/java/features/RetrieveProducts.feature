@RetrieveProduct @All
Feature: Verify retrieving product details from the product-service


  Scenario Outline: Verify retrieving a product by its product id
    Given User calls Add Product API with only ID "<id>"
    Then Status code <statusCode>
    And Verify the response contains "<id>"
    And Verify the response does not contain "name" and "description"
    When User calls Get Product API with the same ID via GET http request
    Then Status code <statusCode>
    And The response should contain the id "<id>"
    Examples:
      | id        | statusCode |
      | AutoTest_ | 200        |

  Scenario Outline: Verify retrieving all products
    When User calls Get All Product API via GET http request
    Then Status code <statusCode>
    And The response schema should match with "ProductCompletePojo.json"
    Examples:
      | statusCode |
      | 200        |

  Scenario Outline: Verify retrieving all products with data types for each parameter
    When User calls Get All Product API via GET http request
    Then Status code <statusCode>
    And The response schema should match with "ProductCompletePojo.json"
    And All the products should contain the "id" param
    And All the id should be of "int" or "string" type
    And All the name should be of "string" or "null/empty" type
    And All the description should be of "string" or "null/empty" type
    Examples:
      | statusCode |
      | 200        |

  Scenario Outline: Verify retrieving a product with a non-existing product id
    When User calls Get Product API with a non-existing product id "1129948400" via GET http request
    Then Status code <statusCode>
    And Verify the response does not contain "name" and "description"
    Examples:
      | statusCode |
      | 200        |


