@RemoveProduct @All
Feature: Verify removing a product details from the product-service


  Scenario Outline: Verify removing a product by its product id
    Given User calls Add Product API with "<id>", "<name>", "<description>" via POST http request
    Then Status code <statusCode>
    And Verify the response contains "<id>", "<name>", "<description>"
    When User calls delete product API with the same ID via DELETE http request
    Then Status code <statusCode>
    And The response should contain the key "<key>" with value <value>
    Examples:
      | id        | name         | description               | statusCode | key | value |
      | AutoProd1 | Auto_Product | product added by autotest | 200        | ok  | 1     |

  Scenario Outline: Verify removing a product by non-existing product id
    When User calls delete product API with a non-existing id "<id>"
    Then Status code <statusCode>
    And The response should contain the key "<key>" with value <value>
    Examples:
      | id        | statusCode | key | value |
      | AutoProd1 | 200        | ok  | 0     |

  Scenario Outline: Verify removing a product by a passing the id only in the path parameter
    Given User calls Add Product API with "<id>", "<name>", "<description>" via POST http request
    Then Status code <statusCode>
    And Verify the response contains "<id>", "<name>", "<description>"
    When User calls delete product API with the same ID only in the path parameter via DELETE http request
    Then Status code <statusCode>
    Examples:
      | id        | name         | description               | statusCode |
      | AutoProd1 | Auto_Product | product added by autotest | 400        |

  Scenario Outline: Verify removing a product by a passing the id only in the request body
    Given User calls Add Product API with "<id>", "<name>", "<description>" via POST http request
    Then Status code 200
    And Verify the response contains "<id>", "<name>", "<description>"
    When User calls delete product API with the same ID only in the request body via DELETE http request
    Then Status code <statusCode>
    Examples:
      | id        | name         | description               | statusCode |
      | AutoProd1 | Auto_Product | product added by autotest | 400        |


