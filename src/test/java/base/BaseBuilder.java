package base;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import utilities.PropertiesReader;

public class BaseBuilder {
	PropertiesReader pro = new PropertiesReader();
	RequestSpecBuilder builder;
	RequestSpecification reqspec;
	Response response;
	PrintStream log;
	
	public RequestSpecification placeSpecBuilder() throws FileNotFoundException {
		builder = new RequestSpecBuilder();
		
		//Read environment variable key from command line and search in config.properties to find the match and set it
		String env = System.getProperty("WSNSHELL_HOME");
		
		if(env == null) {
			System.out.println("No Environment found!!!!!!!!, setting default environment to ==> "+pro.getPropValue("product-services"));
			env = pro.getPropValue("product-services");
		}
		
		if(env.equals("product-services")) {
			builder.setBaseUri(pro.getPropValue("product-services"));
		} 
		else if(env.equalsIgnoreCase("product-engine")) {
			builder.setBaseUri(pro.getPropValue("product-engine"));
		}
		else { 
			builder.setBaseUri(pro.getPropValue("product-services"));
		}
		System.out.println("Building Headers....");
		builder.setContentType("application/json");
		log = new PrintStream(new FileOutputStream("log.txt"));
		builder.addFilter(RequestLoggingFilter.logRequestTo(log));
		builder.addFilter(ResponseLoggingFilter.logResponseTo(log));
		reqspec =  builder.build();	
		return reqspec;
	}
}
