package stepdef;

import base.BaseBuilder;
import io.cucumber.java.en.And;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import pojo.ProductCompletePojo;
import pojo.ProductPojo;
import resources.APIResources;
import utilities.BaseTest;

import java.io.FileNotFoundException;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.not;

public class UpdateProduct extends BaseTest {
    @When("User calls update product API with the same ID via PUT http request by updating name {string} and description {string}")
    public void userCallsUpdateProductAPIWithTheSameIDViaPUTHttpRequestByUpdatingNameAndDescription(String updateName, String updateDescription) throws FileNotFoundException {
        ProductPojo product = new ProductPojo(validateParams.get("id"), updateName, updateDescription);
        validateParams.put("name", updateName);
        validateParams.put("description", updateDescription);

        APIResources resourceAPI = APIResources.UpdateProductAPI;
        reqspec = new BaseBuilder().placeSpecBuilder();

        response = RestAssured.given()
                .spec(reqspec)
                .contentType("application/json")
                .body(product)
                .when()
                .put(resourceAPI.getResource(), validateParams.get("id"));

        logResponse(response.thenReturn());
    }

    @And("The response should contain the name {string} and description {string}")
    public void theResponseShouldContainTheNameAndDescription(String updateName, String updateDescription) {
        ProductCompletePojo product = response.getBody().as(ProductCompletePojo.class);
        assertThat(product.getname(), equalTo(updateName));
        assertThat(product.getdescription(), equalTo(updateDescription));
    }

    @When("User calls update product API with a non-existing id {string}")
    public void userCallsUpdateProductAPIWithANonExistingId(String id) throws FileNotFoundException {
        ProductPojo product = new ProductPojo(id, "non-existing name", "non-existing description");

        APIResources resourceAPI = APIResources.UpdateProductAPI;
        reqspec = new BaseBuilder().placeSpecBuilder();

        response = RestAssured.given()
                .spec(reqspec)
                .contentType("application/json")
                .body(product)
                .when()
                .put(resourceAPI.getResource(), id);

        logResponse(response.thenReturn());
    }

    @And("The response should not contain the id {string}")
    public void theResponseShouldNotContainTheId(String id) {
        ProductCompletePojo product = response.getBody().as(ProductCompletePojo.class);
        assertThat(product.getId().toString(), not(equalTo(id)));
    }
}
