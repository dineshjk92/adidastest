package stepdef;

import base.BaseBuilder;
import io.cucumber.java.en.And;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import pojo.ProductCompletePojo;
import pojo.ProductPojo;
import resources.APIResources;
import utilities.BaseTest;

import java.io.FileNotFoundException;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class RemoveProduct extends BaseTest {
    @When("User calls delete product API with the same ID via DELETE http request")
    public void userCallsDeleteProductAPIWithTheSameIDViaDELETEHttpRequest() throws FileNotFoundException {
        ProductPojo product = new ProductPojo(validateParams.get("id"), "", "");

        APIResources resourceAPI = APIResources.UpdateProductAPI;
        reqspec = new BaseBuilder().placeSpecBuilder();

        response = RestAssured.given()
                .spec(reqspec)
                .contentType("application/json")
                .body(product)
                .when()
                .delete(resourceAPI.getResource(), validateParams.get("id"));

        logResponse(response.thenReturn());
    }

    @And("The response should contain the key {string} with value {int}")
    public void theResponseShouldContainTheKeyWithValue(String key, int value) {
        assertThat(response.getBody().jsonPath().get(key), equalTo(value));
    }

    @When("User calls delete product API with the same ID only in the path parameter via DELETE http request")
    public void userCallsDeleteProductAPIWithTheSameIDOnlyInThePathParameterViaDELETEHttpRequest() throws FileNotFoundException {
        ProductPojo product = new ProductPojo(validateParams.get("id"), "", "");

        APIResources resourceAPI = APIResources.UpdateProductAPI;
        reqspec = new BaseBuilder().placeSpecBuilder();

        response = RestAssured.given()
                .spec(reqspec)
                .contentType("application/json")
                .body(product)
                .when()
                .delete(resourceAPI.getResource(), "");

        logResponse(response.thenReturn());
    }

    @When("User calls delete product API with the same ID only in the request body via DELETE http request")
    public void userCallsDeleteProductAPIWithTheSameIDOnlyInTheRequestBodyViaDELETEHttpRequest() throws FileNotFoundException {
        ProductPojo product = new ProductPojo("", "", "");

        APIResources resourceAPI = APIResources.UpdateProductAPI;
        reqspec = new BaseBuilder().placeSpecBuilder();

        response = RestAssured.given()
                .spec(reqspec)
                .contentType("application/json")
                .body(product)
                .when()
                .delete(resourceAPI.getResource(), validateParams.get("id"));

        logResponse(response.thenReturn());
    }

    @When("User calls delete product API with a non-existing id {string}")
    public void userCallsDeleteProductAPIWithANonExistingId(String id) throws FileNotFoundException {
        ProductPojo product = new ProductPojo(id, "", "");

        APIResources resourceAPI = APIResources.UpdateProductAPI;
        reqspec = new BaseBuilder().placeSpecBuilder();

        response = RestAssured.given()
                .spec(reqspec)
                .contentType("application/json")
                .body(product)
                .when()
                .delete(resourceAPI.getResource(), id);

        logResponse(response.thenReturn());
    }
}
