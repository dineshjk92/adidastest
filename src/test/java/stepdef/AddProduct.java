package stepdef;

import base.BaseBuilder;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.*;

import org.testng.Assert;
import pojo.ProductPojo;
import resources.APIResources;
import utilities.BaseTest;
import utilities.Utilities;

import java.io.FileNotFoundException;
import java.util.HashMap;

public class AddProduct extends BaseTest {

    private static Logger logger = LogManager.getLogger(AddProduct.class);

    @Given("User calls Add Product API with {string}, {string}, {string} via POST http request")
    public void userCallsAddProductAPIWithIdNameDescriptionViaPOSTHttpRequest(String id, String name, String description) throws FileNotFoundException {
        String newID = id + System.currentTimeMillis();
        ProductPojo product = new ProductPojo(newID, name, description);
        validateParams.put("id", newID);
        validateParams.put("name", name);
        validateParams.put("description", description);

        APIResources resourceAPI = APIResources.AddProductAPI;
        reqspec = new BaseBuilder().placeSpecBuilder();

        response = RestAssured.given()
                .spec(reqspec)
                .contentType("application/json")
                .body(product)
                .when()
                .post(resourceAPI.getResource());

        logResponse(response.thenReturn());

    }

    @Then("Status code {int}")
    public void statusCodeIsStatusCode(int statusCode) {
        ResponseSpecification responseSpec = new ResponseSpecBuilder()
                .expectStatusCode(statusCode)
                .expectContentType("application/json")
                .build();

        response.thenReturn().then()
                .spec(responseSpec);
    }

    @And("{string} in response body contains {string}")
    public void inResponseBodyContains(String jsonPath, String value) {
    }

    @And("Verify the response contains {string}, {string}, {string}")
    public void verifyTheResponseContainsInTheResponse(String id, String name, String description) {
        Assert.assertTrue(response.thenReturn().then().extract().path("id").toString().contains(id), "Product IDs in request and response don't match");
        Assert.assertEquals(response.thenReturn().then().extract().path("name"), name, "Product names in request and response don't match");
        Assert.assertEquals(response.thenReturn().then().extract().path("description"), description, "Product descriptions in request and response don't match");
    }

    @When("User calls Get Product API with the same ID via GET http request")
    public void userCallsGetProductAPIWithTheSameIDViaGETHttpRequest() throws FileNotFoundException {
        APIResources resourceAPI = APIResources.GetProductAPI;
        reqspec = new BaseBuilder().placeSpecBuilder();

        response = RestAssured.given()
                .spec(reqspec)
                .contentType("application/json")
                .when()
                .get(resourceAPI.getResource(), validateParams.get("id"));

        logResponse(response.thenReturn());
    }

    @Given("User calls Add Product API with {string} with empty name and description via POST http request")
    public void userCallsAddProductAPIWithViaPOSTHttpRequest(String id) throws FileNotFoundException {
        String newID = id + System.currentTimeMillis();
        ProductPojo product = new ProductPojo(newID, "", "");
        validateParams.put("id", newID);

        APIResources resourceAPI = APIResources.AddProductAPI;
        reqspec = new BaseBuilder().placeSpecBuilder();

        response = RestAssured.given()
                .spec(reqspec)
                .contentType("application/json")
                .body(product)
                .when()
                .post(resourceAPI.getResource());

        logResponse(response.thenReturn());
    }

    @And("Verify the response contains {string}")
    public void verifyTheResponseContainsInTheResponse(String id) {
        Assert.assertTrue(response.thenReturn().then().extract().path("id").toString().contains(id), "Product IDs in request and response don't match");
    }

    @And("Verify the name and description are empty in the response")
    public void verifyTheNameAndDescriptionAreEmptyInTheResponse() {
        Assert.assertEquals(response.thenReturn().then().extract().path("name"), "", "Product names in request and response don't match");
        Assert.assertEquals(response.thenReturn().then().extract().path("description"), "", "Product descriptions in request and response don't match");
    }

    @Given("User calls Add Product API with {string} with null name and description via POST http request")
    public void userCallsAddProductAPIWithWithNullNameAndDescriptionViaPOSTHttpRequest(String id) throws FileNotFoundException {
        String newID = id + System.currentTimeMillis();
        ProductPojo product = new ProductPojo(newID, null, null);
        validateParams.put("id", newID);

        APIResources resourceAPI = APIResources.AddProductAPI;
        reqspec = new BaseBuilder().placeSpecBuilder();

        response = RestAssured.given()
                .spec(reqspec)
                .contentType("application/json")
                .body(product)
                .when()
                .post(resourceAPI.getResource());

        logResponse(response.thenReturn());
    }

    @And("Verify the name and description are null in the response")
    public void verifyTheNameAndDescriptionAreNullInTheResponse() {
        //Assert.assertEquals(response.thenReturn().then().extract().path("name"), null, "Product names in request and response don't match");
        //Assert.assertEquals(response.thenReturn().then().extract().path("description"), null, "Product descriptions in request and response don't match");
    }

    @And("Verify the response does not contain {string}, {string}, {string}")
    public void verifyTheResponseDoesNotContainInTheResponse(String id, String name, String description) {
        Assert.assertFalse(response.thenReturn().then().extract().path("id").toString().contains(id), "Product ID should be present since it was already created");
        Assert.assertNotEquals(response.thenReturn().then().extract().path("name"), name, "Product name should be present since it was already created");
        Assert.assertNotEquals(response.thenReturn().then().extract().path("description"), description, "Product description should be present since it was already created");

    }

    @When("User calls Add Product API with the same details via POST http request")
    public void userCallsAddProductAPIWithTheSameDetailsViaPOSTHttpRequest() throws FileNotFoundException {
        ProductPojo product = new ProductPojo(validateParams.get("id"),
                validateParams.get("name"),
                validateParams.get("description"));

        APIResources resourceAPI = APIResources.AddProductAPI;
        reqspec = new BaseBuilder().placeSpecBuilder();

        response = RestAssured.given()
                .spec(reqspec)
                .contentType("application/json")
                .body(product)
                .when()
                .post(resourceAPI.getResource());

        logResponse(response.thenReturn());
    }

    @Given("User calls Add Product API with only ID {string}")
    public void userCallsAddProductAPIWithOnlyAnd(String id) throws FileNotFoundException {
        String reqBody = "{}";
        id = "AutoTest_" + System.currentTimeMillis();
        validateParams.put("id", id);
        reqBody = "{\n" +
                "  \"id\":  \""+ id +"\"\n" +
                "}";

        APIResources resourceAPI = APIResources.AddProductAPI;
        reqspec = new BaseBuilder().placeSpecBuilder();

        response = RestAssured.given()
                .spec(reqspec)
                .contentType("application/json")
                .body(reqBody)
                .when()
                .post(resourceAPI.getResource());

        logResponse(response.thenReturn());
    }

    @And("Verify the response does not contain {string} and {string}")
    public void verifyTheResponseDoesNotContainAnd(String name, String description) {
        Assert.assertNull(response.thenReturn().then().extract().path(name), "Product name should be null");
        Assert.assertNull(response.thenReturn().then().extract().path(description), "Product descriptions should be null");
    }

    @Given("User calls Add Product API without product id with {string} and {string}")
    public void userCallsAddProductAPIWithoutProductIdWithAnd(String name, String description) throws FileNotFoundException {
        String reqBody = "{\n" +
                "  \"name\": \"" + name + "\",\n" +
                "  \"description\": \"" + description + "\"\n" +
                "}";

        APIResources resourceAPI = APIResources.AddProductAPI;
        reqspec = new BaseBuilder().placeSpecBuilder();

        response = RestAssured.given()
                .spec(reqspec)
                .contentType("application/json")
                .body(reqBody)
                .when()
                .post(resourceAPI.getResource());

        logResponse(response.thenReturn());
    }

    @And("Verify the response does not contain {string}")
    public void verifyTheResponseDoesNotContain(String id) {
        Assert.assertNull(response.thenReturn().then().extract().path(id), "Product id should be null");
    }

}
