package stepdef;

import base.BaseBuilder;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.ResponseOptions;
import io.restassured.specification.RequestSpecification;
import org.testng.Assert;
import pojo.ProductCompletePojo;
import pojo.ProductPojo;
import resources.APIResources;
import utilities.BaseTest;
import utilities.Constants;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.MatcherAssert.assertThat;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;


public class RetrieveProduct extends BaseTest {

    public static String token;
    public static RequestSpecification reqspec;


    @Given("User calls GET operation for {string}")
    public void userCallsGETOperationFor(String id) throws FileNotFoundException {

    }

    @And("The response should contain the id {string}")
    public void theResponseShouldContainTheId(String id) {
        ProductCompletePojo productPojo = response.getBody().as(ProductCompletePojo.class);
        assertThat(productPojo.getId().toString(), equalTo(validateParams.get("id")));

        String responseString = response.getBody().asString();
        assertThat(responseString, matchesJsonSchema(new File(Constants.payloadPath + "ProductCompletePojo.json")));
    }

    @When("User calls Get All Product API via GET http request")
    public void userCallsGetAllProductAPIViaGETHttpRequest() throws FileNotFoundException {
        APIResources resourceAPI = APIResources.GetAllProductAPI;
        reqspec = new BaseBuilder().placeSpecBuilder();

        response = RestAssured.given()
                .spec(reqspec)
                .contentType("application/json")
                .when()
                .get(resourceAPI.getResource());

        logResponse(response.thenReturn());
    }

    @And("The response schema should match with {string}")
    public void theResponseSchemaShouldMatchWith(String payloadFile) {
        String responseString = response.getBody().asString();
        assertThat(responseString, matchesJsonSchema(new File(Constants.payloadPath + payloadFile)));
    }

    @And("All the products should contain the {string} param")
    public void allTheProductsShouldContainTheParam(String param) {
        ProductCompletePojo[] products = response.getBody().as(ProductCompletePojo[].class);
        Assert.assertTrue(Arrays.stream(products)
                .anyMatch(e -> e.getId() != null && e.getId() != ""), "Product doesn't contain ID attribute as expected");
    }

    @And("All the id should be of {string} or {string} type")
    public void allTheShouldBeOfOrType(String type1, String type2) {
        ProductCompletePojo[] products = response.getBody().as(ProductCompletePojo[].class);
        Assert.assertTrue(Arrays.stream(products)
                .anyMatch(e -> e.getId() instanceof Integer || e.getId() instanceof String), "Product doesn't contain ID attribute as expected");
    }

    @And("All the name should be of {string} or {string} type")
    public void allTheNameShouldBeOfOrType(String type1, String type2) {
        ProductCompletePojo[] products = response.getBody().as(ProductCompletePojo[].class);
        Assert.assertTrue(Arrays.stream(products)
                .anyMatch(e -> e.getname() == null || e.getname() == "" || e.getname() instanceof String ), "Product doesn't contain ID attribute as expected");
    }

    @And("All the description should be of {string} or {string} type")
    public void allTheDescriptionShouldBeOfOrType(String type1, String type2) {
        ProductCompletePojo[] products = response.getBody().as(ProductCompletePojo[].class);
        Assert.assertTrue(Arrays.stream(products)
                .anyMatch(e -> e.getdescription() == null || e.getdescription() == "" || e.getdescription() instanceof String ), "Product doesn't contain ID attribute as expected");
    }

    @When("User calls Get Product API with a non-existing product id {string} via GET http request")
    public void userCallsGetProductAPIWithANonExistingProductIdViaGETHttpRequest(String id) throws FileNotFoundException {
        APIResources resourceAPI = APIResources.GetProductAPI;
        reqspec = new BaseBuilder().placeSpecBuilder();

        response = RestAssured.given()
                .spec(reqspec)
                .contentType("application/json")
                .when()
                .get(resourceAPI.getResource(), id);

        logResponse(response.thenReturn());
    }
}
