package utilities;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class Utilities {

	public static JsonPath rawToJson(Response response) {
		JsonPath js = new JsonPath(response.body().asString());
		return js;
	}

	public static String generateStringOfLength(int length) {
		StringBuilder str = new StringBuilder("ABCDEEFGHIJKLMNOPQRSTUVWXYZ");

		if(str.length() > length) {
			return str.substring(0, length);
		} else {
			while(str.length() >= length) {
				str.append(str);
			}
			return str.substring(0, length);
		}

	}
}
