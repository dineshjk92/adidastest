package utilities;

import io.restassured.response.Response;
import io.restassured.response.ResponseOptions;
import io.restassured.specification.RequestSpecification;

import java.util.HashMap;

public class BaseTest {


    public static ResponseOptions<Response> response;
    public static RequestSpecification reqspec;
    public static HashMap<String, String> validateParams = new HashMap<String, String>();

    public void logResponse(Response response) {
        response.prettyPrint();
    }
}
