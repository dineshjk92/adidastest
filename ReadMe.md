# Adidas - QA Test
## _Author : Dinesh J (dineshjk92@gmail.com)_

## To host the product-engine services from docker-compose file
Navigate to `adidasTest\products-docker-composer` folder and run the command `docker-compose up --build -d` to run the services in detached mode

## Automation Framework

The automation framework is designed with Java based Restassured library following BDD style. The API features are bifurcated into different action items and are automated into separete feature files.

The featues files under `adidasTest\src\test\java\features` all feature files and step definition are implemented in `adidasTest\src\test\java\stepdef` folder.

## Execution

In order to execute the tests the test runner available in `adidasTest\src\test\java\testrunner` should be triggered either via command line or via IDE.

For command line execution, run the command `mvn test`

Bugs found during the test,

- On trying to call Get request for a product with a non-exiting ID throws 500 internal server error. 
  - This can be avoided by handling the bad reuqest parameter in the development code logic
- On trying to get a price from product-engine with a non-exiting ID shows some data
  - Expected: it should not show any data as there's no such product. 
- On trying to create a new product without id or empty/null values for id, still works
  - This is a critical bug to be fixed in the application as this ruins the application with bad data in the system. As a result, the system crashes and further api calls fail.
- On trying to create a product with an existing product id still works
  - It created duplicate products in the system damaging the normalization of database resulting into inconsistency
- Retrieving details/updating details/deleting a product with non-existing id, still works with dummy response
  - This again has to be fixed in the code as it makes false promises in the API responses with 200 OK status when there's no such data available in the system
  
  
##Performance Test:

The load test for the GET/POST/PUT/DELETE api calls from product-services are performed through Jmeter. The jmeter script for the same is available under `loadTest\` directory

To execute the tests from command line, run the command from the above folder,  
`<path to jmeter.exe> -n -t productServices.jmx -l reportCSV.csv -e -o reports\`
>Note: the above command holds good for Windows OS. To run from OS X, navigate provide the executable path to Jmeter accordingly`
This generates two reports CSV and HTML reports. 